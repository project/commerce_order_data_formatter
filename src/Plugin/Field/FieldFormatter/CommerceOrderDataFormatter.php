<?php

namespace Drupal\commerce_order_data_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'commerce_order_data_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_order_data_formatter",
 *   label = @Translation("Commerce Order Data (key|value)"),
 *   description = @Translation("Display specific keys value of data field."),
 *   field_types = {
 *     "map"
 *   }
 * )
 */
class CommerceOrderDataFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'keys' => NULL,
      'key_name' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['keys'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#description' => $this->t('Enter a key, or a comma-separated list of keys that should be shown. For example: my_key, test. Leave empty to display all keys values.'),
      '#default_value' => $this->getSetting('keys'),
    ];
    $element['key_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display key name'),
      '#description' => $this->t('Display key name as label for key value'),
      '#default_value' => $this->getSetting('key_name'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $keys = $this->getKeys();
    $summary[] = $this->formatPlural(count($keys), 'Key: @keys', 'Keys: @keys', [
      '@keys' => $keys ? trim($this->getSetting('keys')) : $this->t('All'),
    ]);
    $summary[] = $this->t('Display key name: @value', [
      '@value' => $this->getSetting('key_name') ? $this->t('Yes') : $this->t('No'),
    ]);

    return $summary;
  }

  /**
   * Get fields keys list.
   *
   * @return false|string[]
   *   List with keys.
   */
  protected function getKeys() {
    $value = explode(',', $this->getSetting('keys') ?: '');
    return array_filter(array_map('trim', $value));
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $keys = $this->getKeys();
    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\MapItem $item */
    foreach ($items as $delta => $item) {
      $values = $item->toArray();
      if (empty($keys) || count($keys) > 1) {
        $elements[$delta] = $this->getKeysItemList($values, $keys);
      }
      else {
        $key = reset($keys);
        $value = $values[$key] ?? '';
        $elements[$delta] = $this->viewSingle($key, $value);
      }
    }

    return $elements;
  }

  /**
   * Display a single key - value.
   *
   * @param string $key
   *   Key name.
   * @param mixed $value
   *   Key value.
   *
   * @return array
   *   Element output.
   */
  protected function viewSingle(string $key, $value) {
    if (is_bool($value)) {
      $value = (int) $value;
    }
    elseif (is_array($value)) {
      $value = str_replace('=', ': ', http_build_query($value, NULL, '<br>'));
    }
    if ($this->getSetting('key_name')) {
      $element = [
        '#type' => 'inline_template',
        '#template' => '<b>{{ key }}:</b><br> {{ value }}',
        '#context' => [
          'key' => $key,
          'value' => ['#markup' => $value],
        ],
      ];
    }
    else {
      $element = ['#markup' => $value];
    }
    return $element;
  }

  /**
   * Gets the renderable item list of keys - values.
   *
   * @param array $values
   *   The values list.
   * @param array $keys
   *   The keys list.
   *
   * @return array
   *   The render array.
   */
  protected function getKeysItemList(array $values, array $keys = []) {
    $build = [
      '#theme' => 'item_list',
      '#items' => [],
    ];

    if (!empty($keys)) {
      foreach ($keys as $key) {
        $value = $values[$key] ?? '';
        $build['#items'][$key] = $this->viewSingle($key, $value);
      }
    }
    else {
      foreach ($values as $key => $value) {
        $build['#items'][$key] = $this->viewSingle($key, $value);
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $supported_entity_types = [
      'commerce_order',
      'commerce_order_item',
    ];
    return in_array($field_definition->getTargetEntityTypeId(), $supported_entity_types);
  }

}
