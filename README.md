# Commerce Order Data Formatter

Provides field formatter for commerce order data field that allows displaying
the value of the particular key in this field.

* For a full description of the module, visit:
  [project page](https://www.drupal.org/project/commerce_order_data_formatter)

* To submit bug reports and feature suggestions, or track changes:
  [issue queue](https://www.drupal.org/project/issues/commerce_order_data_formatter)


## Contents of this file

* Requirements
* Installation
* Configuration
* Maintainers


## Requirements

This module requires:
* Field (Drupal core)
* <a href="https://www.drupal.org/project/commerce">Commerce</a> module


## Installation

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Drupal Modules](https://www.drupal.org/documentation/install/modules-themes/modules-8) for
  further information.


## Configuration

- On the order view display or views organize the field as your workflow
  requires.


## Maintainers

Current maintainers:
* Oleksandr Yushchenko - [oleksandr-yushchenko](https://www.drupal.org/user/3559498)
